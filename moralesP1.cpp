//Project: Operating Systems - a simple shell
//Author: Kelly Morales

#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

using namespace std;

void getCommands(vector<string> &commands);
void getOneCommand(string cmd,  char *command[20]);
void singleCommand(vector<string> &commands);
void multipleCommands(vector<string> &commands, int prevPipe[2]);
void syserror(const char *s);

int main(){
  while (true){
    cout << "$ ";
    vector<string> commands;
    getCommands(commands);
    if (commands.size() == 1)
      singleCommand(commands);
    else if (commands.size() > 1){
      int pfd[2];
      if (pipe(pfd) == -1)
        syserror("Failed to create a pipe.");
      multipleCommands(commands, pfd);
    }    
  }
  return 0;
}

void getCommands(vector<string> &commands){
  string cmd;
  getline(cin,cmd);
  stringstream ss;
  ss << cmd;
  string one_cmd;
  while(getline(ss, one_cmd, '|')){
    commands.push_back(one_cmd);
  }
}

void getOneCommand(string cmd, char *command[20]){
  stringstream ss;
  ss << cmd;
  int count = 0;
  string one_arg;
  while(getline(ss, one_arg, ' ')){
    char *cstring = new char[one_arg.size() + 1];
    copy(one_arg.begin(), one_arg.end(), cstring);
    cstring[one_arg.size()] = '\0';
    command[count] = cstring;
    count++;
  } 
  command[count] = '\0'; 
}

void singleCommand(vector<string> &commands){
  char *command[20];
  getOneCommand(commands[0], command);
  pid_t childPID;
  switch(childPID = fork()){
  case -1: syserror("Failed to fork successfully.");
    break;
  case 0: execvp(command[0], command);
    syserror("Unsuccessful execvp.");
    break;
  }
  waitpid(childPID, NULL, 0);
}

void multipleCommands(vector<string> &commands, int prevPipe[2]){
  int pipefds[2*(commands.size() - 1)];
  for (int i = 0; i < (commands.size() - 1); i++){
    if (pipe(pipefds + i * 2) == -1){
      syserror("Failed to create pipe(s).");
    }
  }
  for (int i = 0; i < commands.size(); i++){
    char *command[20];
    getOneCommand(commands[i], command);
    pid_t childPID;
    switch(childPID = fork()){
    case -1: syserror("Failed to fork successfully.");
      break;
    case 0: 
    // pipefds[i - 2] = previous pfd[0]
    // pipefds[i - 1] = previous pfd [1]
    // pipefds[i] = current pfd[0]
    // pipefds[i + 1] = current pfd[1]
      if (i == 0){ // pipe for first command
        if (dup2(pipefds[i - 1], 1) == -1 /*|| close(pipefds[i - 2]) == -1 || close(pipefds[i - 1]) == -1*/)
          syserror("Failed to run dup2 or to close the pipe file-descriptors in first child.");
      }
      else if (i == commands.size() - 1){ // pipe for last command
        if (dup2(pipefds[i], 0) == -1 /*|| close(pipefds[i]) == -1*/)
          syserror("Failed to run dup2 or to close the pipe file-descriptors in last child.");
      }
      else{ // pipe for middle commands
        if (dup2(pipefds[i - 2], 0) == -1 /*|| close(pipefds[i - 2]) == -1 || close(pipefds[i]) == -1 */|| dup2(pipefds[i + 1], 1) == -1 /*|| close(pipefds[i + 1]) == -1*/)
          syserror("Failed to run dup2 or to close the pipe file-descriptors in middle child.");
      }
      for (int j = 0; j < 2 * (commands.size() - 1); j++){
        close(pipefds[j]);
      }
      execvp(command[0], command);
      syserror("Unsuccessful execvp.");
      break;
    }
    for (int i = 0; i < 2 * (commands.size() - 1); i++)
      close(pipefds[i]);
    for (int i = 0; i < commands.size() - 1; i++)
      waitpid(childPID, NULL, 0);
  }
}

void syserror(const char *s){
  extern int errno;
  fprintf( stderr, "%s\n", s );
  fprintf( stderr, " (%s)\n", strerror(errno) );
  exit( 1 );
}
